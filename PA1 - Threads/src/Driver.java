import java.io.FileNotFoundException;
import java.io.PrintStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Kerly Titus
 */
public class Driver {

    /**
     * main class
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*******************************************************************************************************************************************
         * TODO : implement all the operations of main class *
         ******************************************************************************************************************************************/

        // // Uncomment this to write the output to a text file
        // try {
        // PrintStream output = new PrintStream("thirdTest.txt");
        // System.setOut(output);
        // } catch (FileNotFoundException e) {
        // System.out.println("File error");
        // System.exit(0);
        // }

        Network objNetwork = new Network("network"); /* Activate the network */
        objNetwork.start();
        Server objServer = new Server(); /* Start the server */
        objServer.start();
        Client objClient1 = new Client("sending"); /* Start the sending client */
        objClient1.start();
        Client objClient2 = new Client("receiving"); /* Start the receiving client */
        objClient2.start();

        // Ensure that all threads are dead before ending the main thread
        try {
            objNetwork.join();
            objServer.join();
            objClient1.join();
            objClient2.join();
        }

        catch (InterruptedException e) {
            e.printStackTrace();
        }

        /*
         * The reason why there is a difference in the running times of each threads
         * when the program is run 3 times is due to the way the scheduler works. In
         * fact, when calling the Thread.yield() method, the scheduler is informed that
         * the current thread that is running does not have any work to do at the moment
         * and to reschedule the process. However, when the scheduler schedules another
         * thread, there is no guarantee that the current thread will be scheduled to
         * run again since the schedule can schedule any available thread, including the
         * current one.
         * 
         * Therefore, it is normal that in some runs, some threads take longer than
         * other running times since it is possible that when that thread called
         * Thread.yield(), the scheduler assigned that same thread to be run again, even
         * though it had no useful work to do at that moment, which creates overhead.
         */

        // he
    }
}
