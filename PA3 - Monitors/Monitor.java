import java.util.ArrayDeque;

/**
 * Class Monitor
 * To synchronize dining philosophers.
 *
 * @author Serguei A. Mokhov, mokhov@cs.concordia.ca
 */
public class Monitor
{

  private enum STATE {THINKING, EATING, HUNGRY, TALKING, WANT_TO_TALK};
  private int numOfPhilosophers;
  private STATE[] states; //Array that will hold the states of each philosopher
  private boolean philosopherTalking; //boolean to check if a philosopher is currently talking to block others from talking
  private ArrayDeque<Integer> talkingQueue; //need a talking queue to prevent starvation in talking
  private ArrayDeque<Integer> hungryQueue;


	/**
	 * Constructor
	 */
	public Monitor(int piNumberOfPhilosophers)
	{
	  numOfPhilosophers = piNumberOfPhilosophers;
	  states = new STATE[piNumberOfPhilosophers];

	  //initialize all philosophers to start thinking
    for(int i =0; i < piNumberOfPhilosophers ; i++){
      states[i] = STATE.THINKING;
    }

    talkingQueue = new ArrayDeque<Integer>();
    hungryQueue = new ArrayDeque<Integer>();

    //since all philosophers are initially thinking, no one is talking
    philosopherTalking = false;

	}
//
	/*
	 * -------------------------------
	 * User-defined monitor procedures
	 * -------------------------------
	 */

  //method to check if the philosopher that is hungry is allowed to eat
  //in order to be allowed to eat, their neighbors cannot be eating and this philosopher needs to be at the head of
  //the hungry queue
  public synchronized void test(final int index){

    try {
      while(true){
        if(states[(index + 1) % numOfPhilosophers] != STATE.EATING && states[(index + (numOfPhilosophers - 1)) % numOfPhilosophers] != STATE.EATING
          && states[index] == STATE.HUNGRY && hungryQueue.peek() == (index + 1)){ //need to do +1 since the input is index, which is piTID - 1
            states[index] = STATE.EATING;
            break;
        }
        else{
          wait();
        }
      }
    }

    catch (InterruptedException e) {
      e.printStackTrace();
    }
  }



	/**
	 * Grants request (returns) to eat when both chopsticks/forks are available.
	 * Else forces the philosopher to wait()
	 */
	public synchronized void pickUp(final int piTID)
	{
    int index = piTID - 1;
		states[index] = STATE.HUNGRY;

		//add philosopher to the hungry queue
		hungryQueue.add(piTID);

		//test the philosopher
		test(index);

		//if the philosopher passed the test, they are currently eating, so remove them from the hungry queue
		hungryQueue.remove();


  }

	/**
	 * When a given philosopher's done eating, they put the chopstiks/forks down
	 * and let others know they are available.
	 */
	public synchronized void putDown(final int piTID)
	{
	  int index = piTID - 1;
		states[index] = STATE.THINKING;

		//wake up sleeping threads to allow them to test if they are allowed to eat
		notifyAll();

	}

	/**
	 * Only one philopher at a time is allowed to philosophy
	 * (while she is not eating).
	 */
  public synchronized void requestTalk(final int piTID) {
    try {
      states[piTID - 1] = STATE.WANT_TO_TALK;
      talkingQueue.add(piTID);

      //philosopher is only allowed to talk if no one else is talking AND if they are at the head of the talking queue
      while (true) {
        if(philosopherTalking == false && talkingQueue.peek() == piTID && states[piTID - 1] == STATE.WANT_TO_TALK){
          break;
        }
        else{
          wait();
        }
      }
      talkingQueue.remove();
      philosopherTalking = true;
      states[piTID - 1] = STATE.TALKING;
    }
    catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

	/**
	 * When one philosopher is done talking stuff, others
	 * can feel free to start talking.
	 */
	public synchronized void endTalk(final int piTID)
	{
		philosopherTalking = false;
    states[piTID - 1] = STATE.THINKING;

    //wake up sleeping threads to let them check if they are allowed to talk
		notifyAll();
	}
}
// EOF
